class StockCombination {
  setStartDay(startDay){this.startDay = startDay}
  setStartPrice(startPrice){this.startPrice = startPrice}
  setFinalDay(finalDay){this.finalDay = finalDay}
  setFinalPrice(finalPrice){this.finalPrice = finalPrice}
  getProfit(){
    return this.finalPrice - this.startPrice;
  }
  printResult() {
    if(this.getProfit() > 0){
      let result = `${this.getProfit()} (Comprou no dia ${this.startDay} (preço `;
      result += `igual a ${this.startPrice}) e vendeu no dia ${this.finalDay} (preço `;
      result += `igual a ${this.finalPrice}), lucro foi de ${this.finalPrice} – ${this.startPrice} = ${this.getProfit()}`;
      return result;
    }
    else return ` 0 (Nesse caso nenhuma transação deve ser feita, lucro máximoigual a 0)`;
  }
}

const checkStock = (listNum)=>{
  if(!Array.isArray(listNum)){
    return console.log(`${listNum} is not a array`)
  }

  const listCombination = [];
  for (let indexOne = 0; indexOne < listNum.length; indexOne++) {

    for (let indexTwo = indexOne + 1; indexTwo < listNum.length; indexTwo++) {

      const combination = new StockCombination();
      combination.setStartDay(indexOne + 1);
      combination.setStartPrice(listNum[indexOne]);
      combination.setFinalDay(indexTwo + 1);
      combination.setFinalPrice(listNum[indexTwo]);
      listCombination.push(combination);
    }
  }

  let bestProfit;
  listCombination.forEach(combination => {

    if(!bestProfit){bestProfit = combination}
    else if(combination.getProfit() > bestProfit.getProfit()){bestProfit = combination}
  });
  return bestProfit.printResult();
}

const result = checkStock([3,7,8,4,1,3,1]);
console.log(result)