# DESAFIO DESENVOLVEDOR FULLSTACK

## PARTE I - BACKEND

O DESAFIO
Teremos 4 questões para avaliar a capacidade de análise e
resolução de problemas através de algoritimos.
Escolha entre as linguagens python e node.js.
Tente resolver o maior número de questões que
conseguir :)

### Questão 01

Dado um array de números inteiros, retorne os índices dos
dois números de forma que eles se somem a um alvo
específico.

Você pode assumir que cada entrada teria exatamente uma
solução, e você não pode usar o mesmo elemento duas
vezes.

#### EXEMPLO

Dado nums = [2, 7, 11, 15], alvo = 9,
Como nums[0] + nums[1] = 2 + 7 = 9,
return [0, 1].

#### RESPOSTA

```js
const sumArray = (listNum, target)=>{
  if(typeof(target) !== 'number'){
    return console.log(`${target} is not a number`)
  }
  else if(!Array.isArray(listNum)){
    return console.log(`${listNum} is not a array`)
  }

  let result;
  for (let indexOne = 0; indexOne < listNum.length; indexOne++) {

    if(result)break
    for (let indexTwo = 0; indexTwo < listNum.length; indexTwo++) {
      //can't use same element 2 times
      if(indexOne !== indexTwo){
        const sum = listNum[indexOne] + listNum[indexTwo]
        if (sum === target){
          result = [indexOne,indexTwo]
          break
        }
      }
    }
  }
  if(result){
    return result
  }
  return 'Sum not found, try again!'
}

const result = sumArray([3, 5, 7,10,20],8);
console.log(result)
```

### Questão 02

Um bracket é considerado qualquer um dos seguintes caracteres: (, ), {, }, [ ou ].

Dois brackets são considerados um par combinado se o bracket de abertura (isto
é, (, [ou {) ocorre à esquerda de um bracket de fechamento (ou seja,),] ou} do
mesmo tipo exato. Existem três tipos de pares de brackets : [ ], { } e ( ).

Um par de brackets correspondente não é balanceado se o de abertura e o de
fechamento não corresponderem entre si. Por exemplo, { [ ( ] ) } não é balanceado
porque o conteúdo entre {e} não é balanceado. O primeiro bracket inclui o de
abertura, (, e o segundo inclui um bracket de fechamento desbalanceado,].

Dado sequencias de caracteres, determine se cada sequência de brackets é
balanceada. Se uma string estiver balanceada, retorne SIM. Caso contrário, retorne
NAO.

#### EXEMPLO

* {[()]} SIM
* {[(])} NAO
* {{[[(())]]}} SIM

#### RESPOSTA


```js
const isBalanced = (brackets)=>{

  const badResult = 'NAO';
  const goodResult = 'SIM';

  //validating input
  if(typeof(brackets) !== 'string'){
    return badResult;
  }

  const validBracket = new Set(['[',']','{','}','(',')']);
  const listBraskets = brackets.split('');

  //validating braskets
  const even = (element) => validBracket.has(element);
  if(!listBraskets.some(even)){
    return badResult
  }

  //checking balancing
  let isBalanced = true;
  const bracketsMap = {'[':']','{':'}','(':')'};
  for (let index = 0; index < (listBraskets.length/2); index++) {
    const frontElement = listBraskets[index];
    const backElement = listBraskets[listBraskets.length - 1 - index];
    if(bracketsMap[frontElement] !== backElement )isBalanced = false
  }
  return (isBalanced?goodResult:badResult);
}

const result = isBalanced('{[()[]}');
console.log(result)
```

### Questão 03

Digamos que você tenha um array para o qual o elemento i
é o preço de uma determinada ação no dia i.
Se você tivesse permissão para concluir no máximo uma
transação (ou seja, comprar uma e vender uma ação), crie
um algoritmo para encontrar o lucro máximo.

**Note que você não pode vender uma ação antes de
comprar.**

#### EXEMPLO

**Input:** [7,1,5,3,6,4]
**Output:** 5 (Comprou no dia 2 (preço
igual a 1) e vendeu no dia 5 (preço
igual a 6), lucro foi de 6 – 1 = 5

**Input:** [7,6,4,3,1]
**Output:** 0 (Nesse caso nenhuma
transação deve ser feita, lucro máximo
igual a 0)

#### RESPOSTA

```js
class StockCombination {
  setStartDay(startDay){this.startDay = startDay}
  setStartPrice(startPrice){this.startPrice = startPrice}
  setFinalDay(finalDay){this.finalDay = finalDay}
  setFinalPrice(finalPrice){this.finalPrice = finalPrice}
  getProfit(){
    return this.finalPrice - this.startPrice;
  }
  printResult() {
    if(this.getProfit() > 0){
      let result = `${this.getProfit()} (Comprou no dia ${this.startDay} (preço `;
      result += `igual a ${this.startPrice}) e vendeu no dia ${this.finalDay} (preço `;
      result += `igual a ${this.finalPrice}), lucro foi de ${this.finalPrice} – ${this.startPrice} = ${this.getProfit()}`;
      return result;
    }
    else return ` 0 (Nesse caso nenhuma transação deve ser feita, lucro máximoigual a 0)`;
  }
}

const checkStock = (listNum)=>{
  if(!Array.isArray(listNum)){
    return console.log(`${listNum} is not a array`)
  }

  const listCombination = [];
  for (let indexOne = 0; indexOne < listNum.length; indexOne++) {

    for (let indexTwo = indexOne + 1; indexTwo < listNum.length; indexTwo++) {

      const combination = new StockCombination();
      combination.setStartDay(indexOne + 1);
      combination.setStartPrice(listNum[indexOne]);
      combination.setFinalDay(indexTwo + 1);
      combination.setFinalPrice(listNum[indexTwo]);
      listCombination.push(combination);
    }
  }

  let bestProfit;
  listCombination.forEach(combination => {

    if(!bestProfit){bestProfit = combination}
    else if(combination.getProfit() > bestProfit.getProfit()){bestProfit = combination}
  });
  return bestProfit.printResult();
}

const result = checkStock([3,7,8,4,1,3,1]);
console.log(result)
```

### Questão 04

Dados n inteiros não negativos representando um mapa de
elevação onde a largura de cada barra é 1, calcule quanta
água é capaz de reter após a chuva.

#### EXEMPLO

**Input:** [0,1,0,2,1,0,1,3,2,1,2,1]
**Output: 6**

#### RESPOSTA

```js
const checkMapOfElevation = (listNum)=>{
  if(!Array.isArray(listNum)){
    return console.log(`${listNum} is not a array`)
  }
  const maximumHeight = Math.max.apply(null,listNum);

  //counter of the number of spaces that are with water
  let output = 0;

  //Scanning each line
  for (let line = 0; line < maximumHeight; line++) {
    //Scanning each column(skipping the first and last column)
    for (let column = 1; column < listNum.length -1; column++) {

      //there is no block in this area and if the left has a block(or water)
      if(listNum[column] <= line && listNum[column-1] > line){
        //console.log(`line: ${line}  | column: ${column} | column height: ${listNum[column]} | column height left: ${listNum[column-1]} | column height right: ${listNum[column+1]}`)

        // if the right has a block(or water)
        if(listNum[column+1] > line){
          output += 1;
          listNum[column] += 1; //adding water
        }else{
          //scans all spaces on the right
          for (let index = column+1; index < listNum.length; index++) {
            //checks if a block to the right of the line
            if(listNum[index] > line){
              output += 1;
              listNum[column] += 1; //adding water
              break;
            }
          }
        }
      }
    }
  }
  return output
}

// const result = checkMapOfElevation([0,1,0,2,1,0,1,3,2,1,2,1]); //6
// const result = checkMapOfElevation([0,2,0,2,1,0,1,3,2,1,2,1]); //7
// const result = checkMapOfElevation([2,1,0,2,1,0,1,3,2,1,2,1]); //8
// const result = checkMapOfElevation([2,1,0,4,1,0,1,4,2,1,2,1]); //14
const result = checkMapOfElevation([0,3,0,2,1,0,1,3,2,1,2,1]); //12
console.log(result)
```
