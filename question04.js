const checkMapOfElevation = (listNum)=>{
  if(!Array.isArray(listNum)){
    return console.log(`${listNum} is not a array`)
  }
  const maximumHeight = Math.max.apply(null,listNum);

  //counter of the number of spaces that are with water
  let output = 0;

  //Scanning each line
  for (let line = 0; line < maximumHeight; line++) {
    //Scanning each column(skipping the first and last column)
    for (let column = 1; column < listNum.length -1; column++) {

      //there is no block in this area and if the left has a block(or water)
      if(listNum[column] <= line && listNum[column-1] > line){
        //console.log(`line: ${line}  | column: ${column} | column height: ${listNum[column]} | column height left: ${listNum[column-1]} | column height right: ${listNum[column+1]}`)

        // if the right has a block(or water)
        if(listNum[column+1] > line){
          output += 1;
          listNum[column] += 1; //adding water
        }else{
          //scans all spaces on the right
          for (let index = column+1; index < listNum.length; index++) {
            //checks if a block to the right of the line
            if(listNum[index] > line){
              output += 1;
              listNum[column] += 1; //adding water
              break;
            }
          }
        }
      }
    }
  }
  return output
}

// const result = checkMapOfElevation([0,1,0,2,1,0,1,3,2,1,2,1]); //6
// const result = checkMapOfElevation([0,2,0,2,1,0,1,3,2,1,2,1]); //7
// const result = checkMapOfElevation([2,1,0,2,1,0,1,3,2,1,2,1]); //8
// const result = checkMapOfElevation([2,1,0,4,1,0,1,4,2,1,2,1]); //14
const result = checkMapOfElevation([0,3,0,2,1,0,1,3,2,1,2,1]); //12
console.log(result)