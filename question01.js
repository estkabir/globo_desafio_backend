const sumArray = (listNum, target)=>{
  if(typeof(target) !== 'number'){
    return console.log(`${target} is not a number`)
  }
  else if(!Array.isArray(listNum)){
    return console.log(`${listNum} is not a array`)
  }

  let result;
  for (let indexOne = 0; indexOne < listNum.length; indexOne++) {

    if(result)break
    for (let indexTwo = 0; indexTwo < listNum.length; indexTwo++) {
      //can't use same element 2 times
      if(indexOne !== indexTwo){
        const sum = listNum[indexOne] + listNum[indexTwo]
        if (sum === target){
          result = [indexOne,indexTwo]
          break
        }
      }
    }
  }
  if(result){
    return result
  }
  return 'Sum not found, try again!'
}

const result = sumArray([3, 5, 7,10,20],8);
console.log(result)