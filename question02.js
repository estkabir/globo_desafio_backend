const isBalanced = (brackets)=>{

  const badResult = 'NAO';
  const goodResult = 'SIM';

  //validating input
  if(typeof(brackets) !== 'string'){
    return badResult;
  }

  const validBracket = new Set(['[',']','{','}','(',')']);
  const listBraskets = brackets.split('');

  //validating braskets
  const even = (element) => validBracket.has(element);
  if(!listBraskets.some(even)){
    return badResult
  }

  //checking balancing
  let isBalanced = true;
  const bracketsMap = {'[':']','{':'}','(':')'};
  for (let index = 0; index < (listBraskets.length/2); index++) {
    const frontElement = listBraskets[index];
    const backElement = listBraskets[listBraskets.length - 1 - index];
    if(bracketsMap[frontElement] !== backElement )isBalanced = false
  }
  return (isBalanced?goodResult:badResult);
}

const result = isBalanced('{[()[]}');
console.log(result)